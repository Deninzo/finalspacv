package com.example.democonnection;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoconnectionApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoconnectionApplication.class, args);
	}
}
