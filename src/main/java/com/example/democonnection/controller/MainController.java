package com.example.democonnection.controller;

import com.example.democonnection.entity.User;
import com.example.democonnection.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class MainController {

    @Autowired
    private UserRepository userRepository;

//    @RequestMapping(value = "/", method = RequestMethod.GET)
//    public void addUser(@RequestParam Long id, @RequestParam String name) {
//        userRepository.save(new User(id, name));
//    }

    @RequestMapping(value = "/user/{id}", method = RequestMethod.GET)
    public User getUser(@PathVariable("id") Long id) {
        return userRepository.findById(id).get();
    }
}
