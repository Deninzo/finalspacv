package com.example.democonnection.entity;


import javax.persistence.*;

@Entity
public class DetailExperience {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private Integer year;

    @ManyToOne
    @JoinColumn(name = "ExpId")
    private Experience experience;

    public DetailExperience(String name, Integer year, Experience experience) {
        this.name = name;
        this.year = year;
        this.experience = experience;
    }

    public DetailExperience() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public Experience getExperience() {
        return experience;
    }

    public void setExperience(Experience experience) {
        this.experience = experience;
    }
}
