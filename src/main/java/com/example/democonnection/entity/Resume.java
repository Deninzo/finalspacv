package com.example.democonnection.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.List;

@Entity
public class Resume {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private List<Education> educations;

    private List<AdditionalEducation> additionalEducations;

    private List<Technology> technologies;

    private List<Project> projects;

    private List<DetailExperience> detailExperiences;

    private List<Certification> certifications;

    public Resume() {
    }



    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<Education> getEducations() {
        return educations;
    }

    public void setEducations(List<Education> educations) {
        this.educations = educations;
    }


}
