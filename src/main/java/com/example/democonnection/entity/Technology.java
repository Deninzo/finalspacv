package com.example.democonnection.entity;


import javax.persistence.*;

@Entity
public class Technology {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private ProjectRole projectRole;

    @ManyToOne
    @JoinColumn(name = "TypeId")
    private Type type;

    public Technology(String name, Type type) {
        this.name = name;
        this.type = type;
    }

    public Technology(String name) {

        this.name = name;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


}
